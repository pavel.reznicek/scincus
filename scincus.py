#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""Scincus – editor TeXi
Auctor: Pavel Řezníček alias Cigydd <pavel.reznicek@evangnet.cz>
"""
from __future__ import absolute_import
import wx
import sys
import os
import wx.stc
import fenestrina_textificiens_stylata as fts
import res_communes as rc
import folium_initians as fi
import editio_mandatorum as em
import dialogus_quaestionis as dq


os.chdir(sys.path[0])

class ApplicatioScinci(wx.App):
    "Applicatio Scinci. Classa programmatis totius."
    def OnInit(self):
        formularius = FormulariusScinci(rc.nil)
        self.SetTopWindow(formularius)
        return True

    def laqueus_capitalis(self):
        return self.MainLoop()


###############
##   init    ##
###############

basis = rc.ViaNostra + os.sep + "bmp" + os.sep

obi = fi.obi # obiciones scinci

sysenc = rc.sysenc
texenc = rc.texenc

instrmta = {
    101: (
        u"folium novum.gif",
        u"Folium &novum\tCtrl+N",
        u"Novum",
        u"Creare folium novum",
        u"Ctrl+N"
        ),
    102: (
        u"resera.gif",
        u"&Reserare folium\tCtrl+O",
        u"Reserare",
        u"Reserare folium exsistentem",
        u"Ctrl+O"
        ),
    103: (
        u"salva.gif",
        u"&Salvare folium\tCtrl+S",
        u"Salvare",
        u"Salvare folium",
        u"Ctrl+S"
        ),
    104: (
        u"salva.gif",
        u"Salvare folium &ut...\tF2",
        u"Salvare ut",
        u"Salvare folium sub nomine...",
        u"F2"
        ),
    105: (
        u"",
        u"&Concludere folium\tCtrl+W",
        u"Concludere",
        u"Concludere folium",
        u"Ctrl+W"
        ),
    107: (
        u"pressus.gif",
        u"&Pressus\tCtrl+P",
        u"Pressus",
        u"Primare folium",
        u"Ctrl+P"
        ),
    109: (
        u"",
        u"&Finis\tAlt+F4",
        u"Finis Scinci",
        u"Scincum finire",
        u"Alt+F4"
        ),
    201: (
        u"",
        u"&Obesum\tCtrl+B",
        u"Obesum",
        u"Selectionem obesam facere",
        u"Ctrl+B"
        ),
    202: (
        u"",
        u"&Italica\tCtrl+I",
        u"Italica",
        u"Selectionem italicatam facere",
        u"Ctrl+I"
        ),
    203: (
        u"",
        u"&Sublineatum\tCtrl+U",
        u"Sublineatum",
        u"Selectionem sublineare",
        u"Ctrl+U"
        ),
    204: (
        u"",
        u"Ma&china scribens\tCtrl+T",
        u"Machina scribens",
        u"Selectionem in typis machinae scribentis performare",
        u"Ctrl+T"
        ),
    205: (
        u"",
        u"&Mathematica\t$",
        u"Mathematica",
        u"Selectionem in modo mathematico performare - prima „$“",
        u"$"
        ),
    207: (
        u"quaestio.png",
        u"&Quaere textum...\tCtrl+F",
        u"Quaere textum",
        u"Quaere textum in folio reserato",
        u"Ctrl+F"
        ),
    209: (
        u"adda.png",
        u"&Adda mandatum...\tAlt+Enter",
        u"Adda mandatum",
        u"Adda mandatum in tabulam mandatorum",
        u"Alt+Enter"
        ),
    210: (
        u"remove.png",
        u"&Remove mandatum...\tAlt+Delete",
        u"Remove mandatum",
        u"Remove mandatum ex tabula mandatorum",
        u"Alt+Delete"
        ),
    301: (
        u"compilator.gif",
        u"&Compilator\tF9",
        u"Compilator",
        u"Exequere compilatorem",
        u"F9"
        ),
    302: (
        u"inspector.png",
        u"&Inspector\tShift+F9",
        u"Inspector",
        u"Exequere inspectorem",
        u"Shift+F9"
        ),
    304: (
        u"scincus16.gif",
        u"Con&figuratio\tF3",
        u"Configuratio",
        u"Editare folium configurantem et initiantem",
        u"F3"
        ),
    wx.ID_SEPARATOR:(
        u"",
        u"",
        u"",
        u"",
        u""
        )
    }

claves = instrmta.keys()
for i in (104, 105, 109, 201, 202, 203, 204, 205, wx.ID_SEPARATOR):
    claves.remove(i)
claves.sort()



class FormulariusScinci(wx.Frame):
    def __init__(self, parens):
        wx.Frame.__init__(self, parens, title=u"Scincus")

        rc.mandator.LegeMandata()

        self.dr = rc.DialogusReserationis(
            self,
            os.path.dirname(obi.FoliumRecens), 
            os.path.basename(obi.FoliumRecens)
        )
        self.ds = rc.DialogusSalvationis(
            self,
            os.path.dirname(obi.FoliumRecens), 
            os.path.basename(obi.FoliumRecens)
        )

        self.fm = em.FenestrumMandatorum(self)

        szr = self.szr = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(szr)

        self.sb = self.CreateStatusBar()

        tb = self.tb = self.CreateToolBar()
        tb.SetToolBitmapSize((16, 16))

        for instr in claves:
            tup = instrmta[instr]
            via_ad_bitomappam_unicodita = basis + tup[0]
            via_ad_bitomappam_systemica = rc.sysenc(via_ad_bitomappam_unicodita)
            try:
                tb.AddLabelTool(
                    id = instr,
                    label = tup[2],
                    bitmap = wx.Bitmap(via_ad_bitomappam_unicodita),
                    bmpDisabled = wx.NullBitmap,
                    shortHelp = tup[2] + " (" + tup[4] + ")",
                    longHelp = tup[3]
                )
            except Exception as e:
                print "Error cum additione haec iconae bitomappicae:", \
                    via_ad_bitomappam_systemica
                print "Classa erroris:", e.__class__.__name__
                print "Nuntius:", e
                print "basis:", type(basis)
                print "tup[0]:", type(tup[0])

        for i in (3, 5, 7, 10, 13):
            tb.InsertSeparator(i)
            tb.Realize()

        ## electiones

        el = self.el = wx.MenuBar()
        self.SetMenuBar(el)

        elFolium = self.elFolium = wx.Menu()
        elsFolium = self.elsFolium = []
        elEditio = self.elEditio = wx.Menu()
        elsEditio = self.elsEditio = []
        elInstrumenta = self.elInstrumenta = wx.Menu()
        elsInstrumenta = self.elsInstrumenta = []


        self.adda_electionem(elFolium, 101)
        self.adda_electionem(elFolium, 102)
        self.adda_electionem(elFolium, 103)
        self.adda_electionem(elFolium, 104)
        self.adda_electionem(elFolium, 105)
        self.adda_electionem(elFolium, wx.ID_SEPARATOR)
        self.adda_electionem(elFolium, 107)
        self.adda_electionem(elFolium, wx.ID_SEPARATOR)
        self.adda_electionem(elFolium, 109)

        self.adda_electionem(elEditio, 201)
        self.adda_electionem(elEditio, 202)
        self.adda_electionem(elEditio, 203)
        self.adda_electionem(elEditio, 204)
        self.adda_electionem(elEditio, 205)
        self.adda_electionem(elEditio, wx.ID_SEPARATOR)
        self.adda_electionem(elEditio, 207)
        self.adda_electionem(elEditio, wx.ID_SEPARATOR)
        self.adda_electionem(elEditio, 209)
        self.adda_electionem(elEditio, 210)

        self.adda_electionem(elInstrumenta, 301)
        self.adda_electionem(elInstrumenta, 302)
        self.adda_electionem(elInstrumenta, wx.ID_SEPARATOR)
        self.adda_electionem(elInstrumenta, 304)

        for ele in elsFolium + elsEditio + elsInstrumenta:
            id = ele.GetId()
            if id in instrmta.keys():
                if instrmta[id][0] > "":
                    bmp = wx.Bitmap(basis + instrmta[id][0])
                    ele.SetBitmap(bmp)
            if ele in elsFolium:
                elFolium.AppendItem(ele)
            elif ele in elsEditio:
                elEditio.AppendItem(ele)
            elif ele in elsInstrumenta:
                elInstrumenta.AppendItem(ele)

        el.Append(elFolium, u"&Folium")
        el.Append(elEditio, u"&Editio")
        el.Append(elInstrumenta, u"&Instrumenta")

        ###########
        # Eventus #
        ###########

        self.Bind(wx.EVT_MENU, self.folium_novum, id=101)
        self.Bind(wx.EVT_MENU, self.resera, id=102)
        self.Bind(wx.EVT_MENU, self.salva, id=103)
        self.Bind(wx.EVT_MENU, self.salva_ut, id=104)
        self.Bind(wx.EVT_MENU, self.conclude_folium, id=105)
        self.Bind(wx.EVT_MENU, self.pressus, id=107)
        self.Bind(wx.EVT_MENU, self.conclude, id=109)

        self.Bind(wx.EVT_CLOSE, self.cum_conclusione)


        self.Bind(wx.EVT_MENU, self.obesum, id=201)
        self.Bind(wx.EVT_MENU, self.italica, id=202)
        self.Bind(wx.EVT_MENU, self.sublineatum, id=203)
        self.Bind(wx.EVT_MENU, self.machina_scribens, id=204)
        self.Bind(wx.EVT_MENU, self.mathematica, id=205)
        self.Bind(wx.EVT_MENU, self.quaere, id=207)
        self.Bind(wx.EVT_MENU, self.adda_mandatum, id=209)
        self.Bind(wx.EVT_MENU, self.dele_mandatum, id=210)

        self.Bind(wx.EVT_MENU, self.exeque_compilatorem, id=301)
        self.Bind(wx.EVT_MENU, self.exeque_inspectorem, id=302)
        self.Bind(wx.EVT_MENU, self.eda_ini, id=304)

        #obi = ObicionesScinci()
        # Liber fenestrinarum textificientum stylatarum:
        liber = self.liber = fts.Liber(self, sessio=obi.Sessio)
        szr.Add(liber, 1, wx.GROW)

        ###############################
        ## initiatio etiam posterior ##
        ###############################

        bundle = wx.IconBundle()
        bundle.AddIcon(wx.Icon(basis+"scincus16.ico", wx.BITMAP_TYPE_ICO,16,16))
        bundle.AddIcon(wx.Icon(basis+"scincus32.ico", wx.BITMAP_TYPE_ICO,32,32))
        self.SetIcons(bundle)

        self.Position = obi.PositioFenestri
        #self.SetSize(obi.MagnitudoFenestri)
        self.Size = obi.MagnitudoFenestri
        self.Show()


    #####################
    ## definice funkcí ##
    #####################

    def adda_electionem(self, parens, id):
        vysl = wx.MenuItem(parens, id, instrmta[id][1], help=instrmta[id][3])
        if parens == self.elFolium:
            self.elsFolium += [vysl]
        elif parens == self.elInstrumenta:
            self.elsInstrumenta += [vysl]
        elif parens == self.elEditio:
            self.elsEditio += [vysl]
        return vysl

    def folium_novum(self, evt):
        self.liber.AddaPaginam(InscriptioFoliiNovi)
        evt.Skip()

    def resera(self, evt):
        self.nucleus_reserationis(selige=True)
        evt.Skip()


    def nucleus_reserationis(self, folium=u"", selige=False):
        if selige:
            if os.path.exists(folium):
                self.dr.SetPath(folium)
        reserare = self.dr.ShowModal() == wx.ID_OK
        if reserare:
            folium = self.dr.GetPath()
        else:
            reserare = True
            if os.path.exists(folium):
                self.dr.SetPath(folium)

        if reserare:
            self.liber.AddaPaginam(folium)
            return True


    def salva(self, evt):
        self.liber.PaginaCurrens.SalvaFolium()
        evt.Skip()

    def salva_ut(self, evt):
        pc = self.liber.PaginaCurrens
        pc.SalvaFoliumUt(pc.Folium, quaere=True)
        evt.Skip()

    def conclude_folium(self, evt):
        self.liber.ConcludePaginamCurrentem()
        evt.Skip()

    def pressus(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        if wx.Platform == '__WXMSW__':
            pd = wx.PrintData()
            dc = wx.PrinterDC(pd)
        else:
            pdlg = wx.PrintDialog(evt.GetEventObject().GetParent())
            fiat = pdlg.ShowModal()==wx.ID_OK
            if fiat:
                dc = pdlg.GetPrintDC()
            else:
                dc = None
        if dc:
            dc.StartDoc(u"Pressum Scinci: " + self.liber.PaginaCurrens.Folium)
            dc.StartPage()
            fnt = ed.GetFont()
            fnt.SetFaceName("Luxi Mono")
            # 100 characterorum per latitudinem (ac altitudinem?) paginae
            fnt.SetPixelSize(tuple([i / 100 for i in dc.GetSizeTuple()]))
            dc.SetFont(fnt)
            textum = ed.GetText()
            lineae = textum.splitlines()
            ypos = 0
            xpos = dc.GetTextExtent(u"8")[0]*7
            pagina = 0
            l, a = dc.GetSizeTuple()
            ykrok = dc.GetTextExtent(u"Ůy")[1]
            numli = 0
            for li in lineae:
                if ypos == 0:
                    pagina += 1
                    dc.DrawText(
                        u"Folium \u201e%s\u201c, pagina %s"%(
                            self.liber.PaginaCurrens.Folium, pagina
                        ),
                        0, ypos
                    )
                    ypos += ykrok
                    dc.DrawLine(0, ypos, l, ypos)
                numli += 1
                dc.DrawText(str(numli), xpos / 7 * 5 - 
                  dc.GetTextExtent(str(numli))[0], ypos)
                dc.DrawText(li, xpos, ypos)
                ypos += ykrok
                if ypos >= a - ykrok:
                    ypos = 0
                    dc.EndPage()
                    dc.StartPage()
            dc.EndPage()
            dc.EndDoc()
            evt.Skip()

    def conclude(self, evt):
        self.Close()
        evt.Skip()

    def cum_conclusione(self, evt):
        # Si conclusio licet (post salvationes factas vel reiectas):
        #obi = ObicionesScinci()
        obi.Sessio = self.liber.Sessio
        obi.MagnitudoFenestri = self.GetSizeTuple()
        if self.liber.SalvaPaginasOmnes():
            #obi.ini.Flush() # salvatio (?)
            #obi.ini.Save(obi.ViaAdIni)
            obi.ini.Salva(obi.ViaAdIni)
            evt.Skip()
        else: # conclusio non licet:
            evt.Veto()

    #######################
    ##    Editiones    ##
    #######################

    def obesum(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        ed.obesum()
        evt.Skip()
    def italica(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        ed.italica()
        evt.Skip()
    def sublineatum(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        ed.sublineatum()
        evt.Skip()
    def machina_scribens(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        ed.machinaScribens()
        evt.Skip()
    def mathematica(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        ed.mathematica()
        evt.Skip()

    ## Quaestio et commutatio
    def quaere(self, evt):
        d = self.dq = dq.DialogusQuaestionis(self)
        d.quaestio = self.quaestio
        d.commutatio = self.commutatio
        d.commutatio_omnium = self.commutatio_omnium
        d.Show(True)
        evt.Skip()
        return d

    def quaestio(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        catena_quaerenda = self.dq.TextumQuaerendum
        print "catena quaerenda:", catena_quaerenda
        pos, lon = ed.GetCurrentPos(), ed.GetLength()
        #wx.MessageBox("pos: %s, lon: %s"%(pos,lon))
        inventum = ed.FindText(pos, lon, catena_quaerenda, self.dq.Indicationes)
        print "inventum:", inventum
        if inventum > -1:
            ed.SetSelection(
                inventum, 
                inventum + len(catena_quaerenda.encode('utf8'))
            )
            ed.EnsureCaretVisible()
        evt.Skip()
    def commutatio(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        catena_quaerenda = self.dq.TextumQuaerendum
        if catena_quaerenda:
            catena_substituenda = self.dq.TextumCommutandum
            catena_selecta=ed.GetSelectedText()
            if catena_selecta == catena_quaerenda:
                ed.ReplaceSelection(catena_substituenda)
                ed.EnsureCaretVisible()
            else:
                pos, lon = ed.GetCurrentPos(), ed.GetLength()
                inventum = ed.FindText(pos, lon, catena_quaerenda, 
                  self.dq.Indicationes)
                if inventum > -1:
                    ed.SetSelection(
                        inventum, 
                        inventum + len(catena_quaerenda.encode('utf8'))
                    )
                    ed.ReplaceSelection(catena_substituenda)
                    ed.EnsureCaretVisible()
                else:
                    odp=wx.MessageBox(
                        u"Advenibam ad finem texti. Vuls quaeri ab origine?",
                        u"Substitutio",
                        wx.YES_NO
                        )
                    if odp == wx.YES:
                        pos, lon = 0, ed.GetLength()
                        inventum=ed.FindText(pos, lon, catena_quaerenda, 
                          self.dq.Indicationes)
                        if inventum > -1:
                            ed.SetSelection(
                                inventum, 
                                inventum + len(catena_quaerenda.encode('utf8'))
                            )
                            ed.ReplaceSelection(catena_substituenda)
                            ed.EnsureCaretVisible()
        else:
            wx.MessageBox(u"Catena quaerenda indenda est.")
        evt.Skip()
    def commutatio_omnium(self, evt):
        ed = self.liber.PaginaCurrens.Fenestrina
        catena_quaerenda = self.dq.TextumQuaerendum
        if catena_quaerenda:
            catena_substituenda = self.dq.TextumCommutandum
            pos, lon = 0, ed.GetLength()
            inventum = 0
            numerus_substitutionum = 0
            while inventum > -1:
                inventum = ed.FindText(pos, lon, catena_quaerenda, 
                  self.dq.Indicationes)
                if inventum > -1:
                    ed.SetSelection(
                        inventum,
                        inventum + len(catena_quaerenda)
                    )
                    ed.ReplaceSelection(catena_substituenda)
                    pos = inventum + len(catena_quaerenda)
                    numerus_substitutionum += 1
                else:
                    if numerus_substitutionum == 0:
                        nota = u"Textum quaerendum non inventum est."
                    else:
                        nota = u"Omnes locationes texti quaerendi (%s) "\
                            "substitutae sunt." % numerus_substitutionum
                    wx.MessageBox(nota, u"Substitutio", wx.OK)
        else:
            wx.MessageBox(u"Catena quaerenda indenda est.")
        evt.Skip()

    def adda_mandatum(self, evt):
        self.fm.Adda(self.liber.PaginaCurrens.Fenestrina.MandatumCurrens())
        evt.Skip()

    def dele_mandatum(self, evt):
        self.fm.Elige(self.liber.PaginaCurrens.Fenestrina.MandatumCurrens())
        evt.Skip()


    def eda_ini(self, evt):
        conf=FenestrumConfigurationis(self)
        conf.ShowModal()
        evt.Skip()

    def exeque_compilatorem(self, evt):
        if self.liber.PaginaCurrens and \
            hasattr(self.liber.PaginaCurrens, 'Folium'):
            f = self.liber.PaginaCurrens.Folium
        else:
            f = ''
        if f:
            #obi = ObicionesScinci()
            viaOriginalis = sysenc(os.getcwdu())
            #print u"viaOriginalis: %s"%viaOriginalis
            os.chdir(os.path.dirname(sysenc(f)))
            #print u"os.getcwdu(): %s"%os.getcwdu()
            #print u"obi.Compilator: %s"%obi.Compilator
            #print type(obi.Compilator%{'f': os.path.basename(f)})
            #viaIncodita = sysenc(obi.Compilator%{'f': os.path.basename(f)})
            viaIncodita = \
                sysenc(obi.Compilator%{'f': texenc(os.path.basename(f))})
            #print u"viaIncodita: %s"%viaIncodita
            os.system(viaIncodita)
            os.chdir(viaOriginalis)
        evt.Skip()

    def exeque_inspectorem(self, evt):
        #obi = ObicionesScinci()
        if self.liber.PaginaCurrens and \
            hasattr(self.liber.PaginaCurrens, 'Folium'):
            f = self.liber.PaginaCurrens.Folium
        else:
            f = ''
        if f:
            viaOriginalis=sysenc(os.getcwdu())
            os.chdir(os.path.dirname(sysenc(f)))
            #fdvi=(path.splitext(path.basename(f))[0]+'.dvi')
            nomen_basale = os.path.splitext(os.path.basename(f))[0]
            #if path.exists(path.realpath(fdvi)):
            mandatum_inspectoris = sysenc(obi.Inspector%{'f': nomen_basale})
            #if wx.Platform != '__WXMSW__':
            if sys.platform == 'linux2':
                mandatum_inspectoris += sysenc(" &")
            os.system(mandatum_inspectoris)
            #else:
            #  wx.MessageBox(
            #    u"Folium etiam non compilatum est. Exeque compilatorem prior.",
            #    u"Scincus: executio programmatis inspectanti",
            #    wx.ICON_EXCLAMATION
            #    )
        else:
            wx.MessageBox(
                u"Folium „%s“ non exsistit!"%os.path.realpath(f),
                u"Scincus: executio programmatis inspectanti",
                wx.ICON_EXCLAMATION
                )
        os.chdir(viaOriginalis)
        evt.Skip()



#######################
## konfigurační okno ##
#######################

class FenestrumConfigurationis(wx.Dialog):
    def __init__(self, parens):
        wx.Dialog.__init__(
          self, parens, title=u"Editio folii initiantis et configurantis"
          )
        self.SetAutoLayout(True)
        bs = wx.BoxSizer(wx.HORIZONTAL)

        spatium = 5 # spatium universale

        # pressores
        prFiat = self.prFiat = wx.Button(self, wx.ID_OK, u"&Fiat")
        prFiat.SetDefault()
        self.Bind(wx.EVT_BUTTON, self.fiat, prFiat, wx.ID_OK)
        bs.Add(prFiat, 1, wx.GROW)
        bs.Add((spatium, 0), 0, wx.GROW) #addimus spatium

        prCancel = self.prCancel = wx.Button(self, wx.ID_CANCEL, u"&Cancellare")
        bs.Add(prCancel, 1, wx.GROW)

        szr = wx.BoxSizer(wx.VERTICAL)
        szr.Add(bs, 0, wx.ALIGN_BOTTOM | wx.ALIGN_RIGHT | wx.BOTTOM | wx.RIGHT,
          spatium)

        edIni = self.edIni = wx.stc.StyledTextCtrl(self)
        szr.Prepend(edIni, 1, wx.GROW | wx.ALL, spatium)

        self.SetSizer(szr) # designatio fenestri finita est

        edIni.SetLexer(wx.stc.STC_LEX_XML)

        edIni.LoadFile(obi.ViaAdIni)

        edIni.Bind(wx.EVT_CHAR, self.on_char, edIni)

        Styli = obi.Styli

        # stejný výchozí font pro všechny styly
        for i in range(256):
            ColorTexti =     Styli[i]["ColorTexti"    ]
            ColorPosterior = Styli[i]["ColorPosterior"]
            if ColorTexti    !=None:
                r, g, b = ColorTexti
                edIni.StyleSetForeground(i, wx.Colour(r, g, b))
            if ColorPosterior!=None:
                r, g, b = ColorPosterior
                edIni.StyleSetBackground(i, wx.Colour(r, g, b))
            edIni.StyleSetFaceName(i, obi.NomenScripturae)
            edIni.StyleSetSize(i, obi.MagnitudoScripturae)
        # výchozí font pro GetTextExtent
        # (aby to vycházelo stejně jako ve stylech)
        fnt=edIni.GetFont()
        fnt.SetFaceName(obi.NomenScripturae)
        fnt.SetPointSize(obi.MagnitudoScripturae)
        edIni.SetFont(fnt)

        edIni.SetMarginWidth(1, edIni.GetTextExtent("0")[0] * 5 + 5)
        edIni.SetMarginType(1, wx.stc.STC_MARGIN_NUMBER)
        edIni.SetFocus()

    def fiat(self, evt):
        obi.ini.Salva(obi.ViaAdIni)
        self.edIni.SaveFile(obi.ViaAdIni)
        obi.Relege()
        evt.Skip()

    def on_char(self,evt):
        cos = evt.GetUniChar()
        if cos >= 32 and not evt.AltDown() and not evt.ControlDown():
            ch = unichr(cos)
            self.edIni.ReplaceSelection(ch)
        else:
            evt.Skip()





app = ApplicatioScinci(redirect=False)
app.laqueus_capitalis()
