#!/usr/bin/env python
# -*- coding: utf8 -*-

from res_communes import *
import wx
import wx.gizmos as giz
import locale

class FenestrumMandatorum(wx.Dialog,obiectum):
  def __init__(
    ego,parens,
    stylus=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER,
    titulus=u'Tabula mandatorum compleandorum'
    ):
    wx.Dialog.__init__(ego,parent=parens,style=stylus,title=titulus)
    
    # relator
    rel=ego.szr=wx.BoxSizer(wx.VERTICAL)
    ego.SetSizer(rel)
    
    # tabula mandata edans
    tab=ego.tabula=giz.EditableListBox(ego,label=u'Mandata compleanda')
    rel.Add(tab,1,wx.GROW)
    
    # relator pressorum
    relpr=ego.CreateStdDialogButtonSizer(wx.OK|wx.CANCEL)
    rel.Add(relpr,0,wx.ALIGN_RIGHT|wx.ALL,4)
    
    # pressores
    prFiat=ego.prFiat=relpr.GetAffirmativeButton()
    prFiat.Bind(wx.EVT_BUTTON,ego.Fiat)
    prCancellare=ego.prCancellare=relpr.GetCancelButton()
    
    ego.Layout()
  
  def Fiat(ego,evt):
    tabc=ego.tabula.GetListCtrl()
    tab=[tabc.GetItemText(i) for i in range(tabc.GetItemCount())]
    tab=filter(lambda x: x!='',tab)
    tab.sort(locale.strcoll)
    # unificamus occurentiones mandatorum in tabula
    tmptab=[]
    for it in tab:
      if it not in tmptab:
        tmptab.append(it)
    tab=tmptab
    mandator.TabulaMandatorum = tab
    mandator.Mandata = u''
    for mandatum in mandator.TabulaMandatorum:
        if mandatum:
            mandator.Mandata += mandatum + chr(11)
    folium=open(ViaAdDataMandatorum, 'wb+')
    folium.write(u'\n'.join(mandator.TabulaMandatorum))
    folium.close()
    ego.SetReturnCode(wx.ID_OK)
    ego.Close()
    evt.Skip()
    
  def Adda(ego, mandatum):
    if mandatum:
      if mandatum not in mandator.TabulaMandatorum:
        mandator.TabulaMandatorum.append(mandatum)
      mandator.TabulaMandatorum.sort(locale.strcoll)
      tabc=ego.tabula.GetListCtrl()
      tabc.DeleteAllItems()
      for man in mandator.TabulaMandatorum:
        tabc.Append([man])
      tabc.Append([u''])
      ind=tabc.FindItem(0,mandatum)
      tabc.EnsureVisible(ind)
      tabc.Select(ind)
      tabc.Focus(ind)
      tabc.EditLabel(ind)
      ego.ShowModal()
      
  def Elige(ego, mandatum):
    if mandatum:
      tabc=ego.tabula.GetListCtrl()
      tabc.DeleteAllItems()
      for man in mandator.TabulaMandatorum:
        tabc.Append([man])
      tabc.Append([u''])
      ind=tabc.FindItem(0, mandatum, True)
      tabc.EnsureVisible(ind)
      tabc.Select(ind)
      tabc.Focus(ind)
      ego.ShowModal()
  