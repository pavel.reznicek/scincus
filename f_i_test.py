import wx 
import wx.xrc 
from res_communes import * 
class XMLConfig(wx.xrc.XmlDocument): 
  def __init__(ego, via): 
    wx.xrc.XmlDocument.__init__(ego, via) 
    ego._root = ego.Root 
    #print ego._root 
  def GetChildByName(ego, node, name): 
    if not node: 
      return None 
    children = node.GetChildren() 
    #for child in children: 
    child = children#.GetFirst() 
    while child: 
      if hasattr(child, "Name"): 
        if child.Name == name: 
          print child.Name 
          return child 
      child = child.GetNext() 
  def GetPathNode(ego, via): 
    partes = via.split(u'/') 
    parens = ego._root # Mech 
    for pars in partes[1:]: 
      infans = ego.GetChildByName(parens, pars) 
      parens = infans 
      if not infans: 
        break 
    if infans: 
      return infans 
  def CreatePathNode(ego, via): 
    partes = via.split('/') 
    parent = ego._root 
    for part in partes[1:]: 
      child = ego.GetChildByName(parent, part) 
      if not child: 
        child = wx.xrc.XmlNode(parent, 
          type=wx.xrc.XML_ELEMENT_NODE,name=part 
          ) 
        #child = wx.xrc.XmlNodeEasy(wx.xrc.XML_ELEMENT_NODE, part) 
        #parent.AddChild(child) 
      parent = child 
    return child 
  def Lege(ego, via, eveniens=u""): 
    node = ego.GetPathNode(via) 
    if node: 
      print node.GetNodeContent() 
      return node.GetNodeContent() 
    else: 
      return eveniens 
  def Scribe(ego, via, contentum): 
    node = ego.CreatePathNode(via) 
    #node = ego.GetPathNode(via) 
    if node: 
      print node, node.Name 
      #node.Content = contens 
      txt = wx.xrc.XmlNodeEasy(type=wx.xrc.XML_TEXT_NODE, 
name="Textus") 
      txt.Content = contentum 
      node.AddChild(txt) 
      #node.AddChild(wx.xrc.XmlNodeEasy( 
      #  type=wx.xrc.XML_TEXT_NODE, name="Textus")) 
      return True 
    else: 
      return False 
cfg = XMLConfig(u"settings.xml") 
cfg.Scribe(u"/Setting", u"value") 
