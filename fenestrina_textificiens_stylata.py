#!/usr/bin/env python
# -*- coding: utf8 -*-

import wx
from wx import stc
import os
import os.path as path
from res_communes import *
from folium_initians import *

class FenestrinaTextificiensStylata(stc.StyledTextCtrl,obiectum):
  def __init__(ego, parens):
    stc.StyledTextCtrl.__init__(ego, parens)
    ego.AutoCompSetSeparator(11)
    #ego.AutoCompSetAutoHide(False)
    #ego.AutoCompSetDropRestOfWord(False)
    ego.AutoCompSetDropRestOfWord(True)
    ego.AutoCompSetCancelAtStart(False)
    
    ##ego.AutoCompSetFillUps(SigniNonAlphanumerici)
    ego.SetLexer(wx.stc.STC_LEX_TEX)
    
    ego._ObiciaNominemScripturae(obi.NomenScripturae)
    ego._ObiciaMagnitudinemScripturae(obi.MagnitudoScripturae)
    ego.ObiciaStylos(obi.Styli)


    ego.SetMarginType(1,wx.stc.STC_MARGIN_NUMBER)
    
    ego.SetEdgeColumn(80)
    ego.SetEdgeMode(wx.stc.STC_EDGE_LINE)
    
    
    ego.SetTabIndents(True)
    ego.SetUseTabs(False)
    ego.SetBackSpaceUnIndents(True)
    ego.SetTabWidth(2)
    
    ego._folium=u''
    

    ego.Bind(wx.EVT_CHAR,ego.OnChar)
    ego.Bind(wx.EVT_KEY_DOWN,ego.OnKeyDown)
    
  
  def ReseraFolium(ego,via):
    if via==InscriptioFoliiNovi:
      ego.Clear()
      ego.SetSavePoint()
      ego._folium=u''
      return True
    else:
      if not path.exists(via):
        wx.MessageBox(u'Error in reseratione folii\n"%s":\nFolium non exsistit.'\
          %(via),u'Scink: Error')
        return False
      else:
        try:
          folium=open(via,'rbU+')
          textum_folii_originalis=folium.read()
          folium.close()
          textum_folii_decoditum=\
            textum_folii_originalis#.decode('cp1250').encode('utf8')
          ego.SetTextRaw(textum_folii_decoditum)
          ego.SetSavePoint()
          ego._folium=via
          return True
        except:
          wx.MessageBox(
            u'Error in reseratione folii\n"%s":\nReserare hoc folium'
            u' non possibile est.\nDocumentatio erroris ait:\n%s'\
              %(via,sys.exc_info()[1]),
            u'Scincus: Error'
            )
          return False
        
  def _DaFolium(ego):
    return ego._folium

  Folium=property(_DaFolium,ReseraFolium,doc=u'Da sive fac folium.')

  def SalvaFolium(ego):
    if ego.GetModify():
      if ego._folium==u''\
      or ego._folium==InscriptioFoliiNovi:
        DirectoriumFoliiRecentis = path.dirname(obi.FoliumRecens)
        if path.exists(DirectoriumFoliiRecentis)\
        and path.isdir(DirectoriumFoliiRecentis):
          ViaAdFoliumNovum = DirectoriumFoliiRecentis+os.sep+u'folium_novum.tex'
        else:
          ViaAdFoliumNovum = u'folium_novum.tex' # simplice :-)
        return ego.SalvaFoliumUt(ViaAdFoliumNovum, quaere=True)
      else:
        return ego.SalvaFoliumUt(ego._folium,quaere=False)
    else:
      return True
  def SalvaFoliumUt(ego,nomen,quaere=False):
    if nomen==u''\
    or nomen==InscriptioFoliiNovi:
      nomen==ego._folium
      quaere==True
    if nomen==u''\
    or nomen==InscriptioFoliiNovi:
      nomen=path.dirname(obi.FoliumRecens)+os.sep+u'folium_novum.tex'
      quaere=True
    nomen=\
      path.abspath(
        path.realpath(
          path.expanduser(
            path.expandvars(
              nomen
            )
          )
        )
      )
    if path.exists(path.dirname(nomen)):
      if quaere:
        fs=DialogusSalvationis(ego,path.dirname(nomen),path.basename(nomen))
        resultum=fs.ShowModal()
        continua=resultum==wx.ID_OK
        if continua:
          nomen=fs.GetPath()
      else:
        continua=True
      if continua:
        tf=ego.GetText() # textum folii
        tfi=tf#.encode('cp1250') # textum folii incoditum
        try:
          f=open(nomen,'wb+')
          f.write(tfi)
          f.close()
          ego._folium=nomen
          ego.SetSavePoint()
          return True
        except:
          wx.MessageBox(u'Error apud salvationem folii.\n'
            u'Pytho ait:\n'+sys.exc_info()[1],
            u'Error in Scinco', wx.OK|wx.ICON_ERROR)
          return False
      else: # ne continuas
        return False
    else:
      wx.MessageBox(u'Via ad folium „%s“ non exsistit.\n'
        u'Depreco te folium eum in directorium exsistentem '
        u'salvare.' % nomen,
        u'Error in Scinco', wx.OK|wx.ICON_EXCLAMATION)
      return False

  
  ###########
  ## styli ##
  ###########
  
  def _DaNominemScipturae(ego):
    return ego.GetFont().GetFaceName()
  def _ObiciaNominemScripturae(ego,nomen):
    # nomen deest -> damus Curiorem
    if not nomen:
      nomen = u'Courier'
    # fons initialis idem pro omnes stylos
    for i in range(256):
      ego.StyleSetFaceName(i,nomen)
    fnt=ego.GetFont()
    fnt.SetFaceName(nomen)
    ego.SetFont(fnt)
    # margis putat relatationem ad quinque numeros
    ego.SetMarginWidth(1,ego.GetTextExtent("0")[0]*5+5)
  NomenScripturae=property(_DaNominemScipturae,_ObiciaNominemScripturae)

  def _DaMagnitudinemScipturae(ego):
    return ego.GetFont().GetPointSize()
  def _ObiciaMagnitudinemScripturae(ego,magnitudo):
    # fons initialis idem pro omnes stylos
    for i in range(256):
      ego.StyleSetSize(i,magnitudo)
    fnt=ego.GetFont()
    fnt.SetPointSize(magnitudo)
    ego.SetFont(fnt)
    # margis putat relatationem ad quinque numeros
    ego.SetMarginWidth(1,ego.GetTextExtent("0")[0]*5+5)
  MagnitudoScripturae=property(
    _DaMagnitudinemScipturae,_ObiciaMagnitudinemScripturae
    )
    
  def ObiciaStylos(ego,styli):
    for i in range(len(styli)):
      stylus=styli[i]
      ColorTexti,ColorPosterior=stylus[u'ColorTexti'],stylus[u'ColorPosterior']
      if ColorTexti!=None:
        ego.StyleSetForeground(i,ColorTexti)
      if ColorPosterior!=None:
        ego.StyleSetBackground(i,ColorPosterior)
  
  #####################
  ## res editationis ##
  #####################
  
  def FractioProxima(ego):
    txLi=ego.GetCurLine()[0]
    posInLi=ego.GetColumn(ego.GetCurrentPos())
    posFrac=txLi.rfind(u"\\",0,posInLi)
    return posFrac

  def SignumNonAlphanumericumAntePositionem(ego):
    txLi=ego.GetCurLine()[0]
    posInLi=ego.GetColumn(ego.GetCurrentPos())
    #posFrac=ego.FractioProxima()
    pos=posInLi-1
    if pos>=0 and txLi: sig=txLi[pos]
    else: sig=''
    while (sig in SignaAlphanumerica) and pos>=0:
        pos-=1
        sig=txLi[pos]
    else:
        if pos<0:
            return(-1,"")
    return(pos,sig)

  def SignumNonAlphanumericumPostPositionem(ego):
    txLi=ego.GetCurLine()[0]
    posInLi=ego.GetColumn(ego.GetCurrentPos())
    #posFrac=ego.FractioProxima()
##    pos=posInLi
##    if pos<len(txLi) and txLi: sig=txLi[pos]
##    else: sig=''
##    while (sig in SigniAlphanumerici) and pos<len(txLi)-1:
##        pos+=1
##        sig=txLi[pos]
##    else:
##        if pos<0:
##            return(-1,"")
##    return(pos,sig)
    for pos in range(posInLi,len(txLi)):
      sig=txLi[pos]
      if sig not in SignaAlphanumerica:
        return (pos,sig)
    else: # venimus ad finem lineae
      return (len(txLi),u'')

  def SignumNonAlphaicumAntePositionem(ego):
    txLi=ego.GetCurLine()[0]
    posInLi=ego.GetColumn(ego.GetCurrentPos())
    #posFrac=ego.FractioProxima()
    pos=posInLi-1
    if pos>=0 and txLi: sig=txLi[pos]
    else: sig=''
    while (sig in SignaAlphaica) and pos>=0:
        pos-=1
        sig=txLi[pos]
    else:
        if pos<0:
            return(-1,"")
    return(pos,sig)

  def SignumNonAlphaicumPostPositionem(ego):
    txLi=ego.GetCurLine()[0]
    posInLi=ego.GetColumn(ego.GetCurrentPos())
    #posFrac=ego.FractioProxima()
    for pos in range(posInLi,len(txLi)):
      sig=txLi[pos]
      if sig not in SignaAlphaica:
        return (pos,sig)
    else: # venimus ad finem lineae
      return (len(txLi),u'')

  
  def MandatumCurrens(ego):
    txLi=ego.GetCurLine()[0]
    posInLi=ego.GetColumn(ego.GetCurrentPos())
    posa,siga=ego.SignumNonAlphaicumAntePositionem()
    if siga==u'\\':
      posp,sigp=ego.SignumNonAlphaicumPostPositionem()
      if posp>=posInLi:
        mandatum=txLi[posa:posp]
      else:
        mandatum=u''
    else:
      mandatum=u''
    return mandatum

  def PositioCoordinata(ego):
    return (ego.GetCurLine()[1],ego.GetCurrentLine())
  def PositioInLinea(ego):
    return ego.GetCurLine()[1]
  def LineaPositionis(ego):    
    return ego.GetCurrentLine()
  def TextumLineae(ego,numerus=None):
    if numerus==None:
      numerus=ego.LineaPositionis()
    return ego.GetLine(numerus)
  

  
  # eventus

  def OnKeyDown(ego,evt):
    #print evt.GetUniChar(),evt.GetRawKeyFlags(),evt.GetRawKeyCode()
    kc=evt.GetRawKeyCode()
    fl=evt.GetRawKeyFlags()
    if kc==32\
    and not evt.AltDown()\
    and not evt.ShiftDown()\
    and evt.ControlDown(): # Ctrl+spatium (selectio verbi)
      #wx.MessageBox(str(SignumNonAlphanumericumAntePositionem()))
      pos,sig=ego.SignumNonAlphanumericumAntePositionem()
      print pos,sig
      if pos>=0 and sig==u"\\":
        ego.AutoCompShow(ego.PositioInLinea() - pos, mandator.Mandata)
        rng=ego.TextumLineae()[pos:ego.PositioInLinea()]
        print rng
        for man in mandator.TabulaMandatorum:
          if man.startswith(rng):
            if ego.AutoCompActive():
              ego.AutoCompSelect(rng)
              break
    else:
      if ego.AutoCompActive():
        doleva,doprava=0xff51,0xff53
        start=ego.GetCurrentPos()
        if kc in(doleva,doprava):
          if kc==doleva:
            ego.SetSelection(start-1,start-1)
          elif kc==doprava:
            ego.SetSelection(start+1,start+1)
          pos=ego.GetCurrentPos()
          posSNA,SNA=ego.SignumNonAlphanumericumAntePositionem()
          x,y=ego.PositioCoordinata()
          if fl==doprava and posSNA==x-1:
            ego.AutoCompCancel()
          elif fl==doleva and (SNA!=u'\\' and posSNA>x):
            ego.AutoCompCancel()
          else:
            origo=ego.GetColumn(ego.AutoCompPosStart())
            origo=ego.FractioProxima()
            if origo>-1:
              finis=x
              rng=ego.TextumLineae()[origo:finis]
              print rng
              ego.AutoCompSelect(rng)
        else:
          evt.Skip()
      else:
        evt.Skip()
        
  def OnChar(ego,evt):
    ch=evt.GetUniChar()
    fl=evt.GetRawKeyFlags()
    rkc=evt.GetRawKeyCode()
    #print ch,rkc,fl,rkc
    zn=unichr(ch)#.encode("cp1250")
    if ch>32 and (not evt.AltDown()) and (not evt.ControlDown()):
      #print zn
      if zn==u"\\" :
        ego.AutoCompShow(0, mandator.Mandata)
        evt.Skip()
      elif zn==u"$": # chablonna mathematica
        ego.Mathematica()
##      # in causa wxPythonis < 2.5.4.1
##      elif wx.VERSION[:4]<(2,5,4,1) \
##      and (zn in u"ěĚšŠčČřŘžŽďĎťŤňŇůŮˇ°ľĽĺĹśŚŕŔźŹńŃ"):
      elif zn in u"ěĚšŠčČřŘžŽďĎťŤňŇůŮˇ°ľĽĺĹśŚŕŔźŹńŃ":
        #if numPadNumError():
        #    pass
        #else:
        if ego.GetOvertype():
          if ego.GetSelectedText()==u"":
            origo,finis=ego.GetSelection()
            ego.SetSelection(origo,finis+1)
            # pokud jsme na konci řádku: nepřepisovat
            if ego.GetSelectedText() in "\r\n":
              ego.SetSelection(origo,finis)
          ego.ReplaceSelection(zn)
        else:
          ego.ReplaceSelection(zn)
      else:
        evt.Skip()
      if ego.AutoCompActive():
        ego.AutoCompSelect(
          ego.GetTextRange(
            ego.AutoCompPosStart(),
            ego.GetCurrentPos()
          )
        )
    else:
      evt.Skip()
      
  #######################
  ##    Editationes    ##
  #######################
  
  def chablonna(ego,ante,post):
    sel=ego.GetSelection()
    seltext=ego.GetSelectedText()
    posante=sel[0]
    ego.InsertText(posante,ante)
    pospost=sel[1]+len(ante)
    ego.InsertText(pospost,post)
    ego.SetSelection(posante+len(ante),pospost)
  
  def obesum(ego):
      ego.chablonna(u"\\textbf{",u"}")
      #evt.Skip()
  def italica(ego):
      ego.chablonna(u"\\textit{",u"}")
      #evt.Skip()
  def sublineatum(ego):
      ego.chablonna(u"\\underline{",u"}")
      #evt.Skip()
  def machinaScribens(ego):
      ego.chablonna(u"\\texttt{",u"}")
      #evt.Skip()
  def mathematica(ego):
      ego.chablonna(u"$",u"$")
      #evt.Skip()


class Pagina(wx.Panel,obiectum):
  def __init__(ego,parens,stylus=0):
    wx.Panel.__init__(ego,parens,style=stylus)
    ed=ego._fenestrina=FenestrinaTextificiensStylata(ego)
    szr=ego.szr=wx.BoxSizer(wx.VERTICAL)
    szr.Add(ed,1,wx.GROW)
    panellum_status=ego.PanellumStatus=wx.StatusBar(ego)
    szr.Add(panellum_status,0,wx.GROW)
    ego.SetSizer(szr)
    ed.Bind(wx.EVT_MOTION,ego.OnMouseEnter)

  def _DaFenestrinam(ego):
    return ego._fenestrina
  Fenestrina=property(fget=_DaFenestrinam,fset=None)
  def ReseraFolium(ego,folium):
    successum=ego._fenestrina.ReseraFolium(folium)
    ego.RenovaInscriptionem()
    return successum
  def _DaFolium(ego):
    return ego._fenestrina._DaFolium()
  def _DaDocFolii(ego):
    return ego.Fenestrina.__doc__
  Folium=property(_DaFolium,ReseraFolium,
      doc=_DaDocFolii)

  def SalvaFolium(ego):
    return ego._fenestrina.SalvaFolium()
  def SalvaFoliumUt(ego,nomen,quaere=False):
    return ego._fenestrina.SalvaFoliumUt(nomen,quaere=quaere)
  def SalvaAnteConclusionem(ego):
    if ego._fenestrina.GetModify():
      responsum=wx.MessageBox(u'Vuls salvare modificationes factas in folio \n'
        '%s%s%s?'%(oq,ego._DaFolium(),fq),u"Conclusio folii",
        wx.YES|wx.YES_DEFAULT|wx.NO|wx.CANCEL|wx.ICON_EXCLAMATION)
      if responsum==wx.YES:
        return ego.SalvaFolium()
      elif responsum==wx.NO:
        return True
      else:
        return False
    else:
      return True
  def Conclude(ego):
    potest_concludere=ego.SalvaAnteConclusionem()
    if potest_concludere:
      ego.Destroy()
    return potest_concludere
  
  def OnMouseEnter(ego,evt):
    ego.RenovaInscriptionem()
  def RenovaInscriptionem(ego):
      if ego.PanellumStatus.GetStatusText()<>ego.Folium:
        ego.PanellumStatus.SetStatusText(ego.Folium)

class Liber(wx.Notebook,obiectum):
  def __init__(ego, parens, sessio=[], stylus=0):
    wx.Notebook.__init__(ego,parens,style=stylus)
    ego._paginae=()
    ego._ReseraSessionem(sessio)
  def _DaPaginas(ego):
    return ego._paginae
  Paginae=property(fget=_DaPaginas,fset=None)

  def AddaPaginam(ego, folium, stylus=0, elige=True):
##    try:
    pa = Pagina(ego,stylus)
    successum=pa.ReseraFolium(folium)
    if successum:
      ego.AddPage(pa, path.basename(folium), select=elige)
      if elige:
        ego._EligePaginam(pa)
      ego._paginae += (pa,)
      return pa
    else:
      pa.Destroy()
##    except:
##      if 'pa' in dir():
##        pa.Destroy()
##      return None
  def ConcludePaginam(ego,pagina):
    if pagina:
      concludenda = pagina.SalvaAnteConclusionem()
      if concludenda:
        if len(ego._paginae) == 1:
          ego.AddaPaginam(InscriptioFoliiNovi, elige=False)
        # removemus paginam delendam ex tabula paginarum
        ego._paginae=tuple([p for p in ego._paginae if p!=pagina])
        ego.DeletePage(ego.GetSelection())
    else:
      concludenda = True
    return concludenda
  
  def ConcludePaginamCurrentem(ego):
    return ego.ConcludePaginam(ego._DaPaginamCurrentem())
  
  def SalvaPaginasOmnes(ego):
    pae=ego._paginae
    omnes_concludendae=True
    for pa in pae:
      concludenda = pa.SalvaAnteConclusionem()
      omnes_concludendae &= concludenda
    return omnes_concludendae
  
  def _DaPaginamCurrentem(ego):
    return ego.GetCurrentPage()
  def _EligePaginam(ego,pagina):
    pagina.Fenestrina.SetFocus()
  PaginaCurrens=property(_DaPaginamCurrentem,_EligePaginam)
  
  def _DaSessionem(ego):
    sessio=[pa.Folium for pa in ego._paginae if path.exists(pa.Folium)]
    return sessio
  def _ReseraSessionem(ego,sessio):
    if sessio:
      for fol in sessio:
        ego.AddaPaginam(fol)
    else:
      ego.AddaPaginam(InscriptioFoliiNovi)
  Sessio=property(_DaSessionem,_ReseraSessionem)