#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from res_communes import *
from cigydd.properties import Property

class DialogusQuaestionis(wx.Dialog, obiectum):
  def __init__(ego, parens):
    wx.Dialog.__init__(ego, parens, title=u"Quaestio et commutatio")
    
    ego.Parens = parens
    
    spatium = 8 # spatium commune
    
    ScalatorMarginalis = wx.BoxSizer(wx.VERTICAL)
    ego.SetSizer(ScalatorMarginalis)

    ScalatorCapitalis = wx.BoxSizer(wx.HORIZONTAL)
    ScalatorMarginalis.Add(ScalatorCapitalis, 1, wx.GROW | wx.ALL, spatium)
    
    ScalatorSinister = wx.BoxSizer(wx.VERTICAL)
    ScalatorCapitalis.Add(ScalatorSinister, 3, wx.GROW)
    
    ScalatorQuaestionis = wx.BoxSizer(wx.HORIZONTAL)
    ScalatorSinister.Add(ScalatorQuaestionis, 1, wx.GROW)

    # ins* = inscriptio
    insQuaere = ego.insQuaere = wx.StaticText(ego, label=u"Q&uaere:")
    ScalatorQuaestionis.Add(insQuaere, 1, wx.ALIGN_CENTER_VERTICAL)
    
    ScalatorQuaestionis.Add((spatium, 0))

    edQuaere = ego.edQuaere = wx.TextCtrl(ego)
    ScalatorQuaestionis.Add(edQuaere, 2, wx.GROW)
    
    ScalatorSinister.Add((0, spatium))
    
    ScalatorCommutationis = wx.BoxSizer(wx.HORIZONTAL)
    ScalatorSinister.Add(ScalatorCommutationis, 1, wx.GROW)
    
    insCommuta = ego.insCommuta = wx.StaticText(ego, label=u"Com&muta cum:")
    ScalatorCommutationis.Add(insCommuta, 1, wx.ALIGN_CENTER_VERTICAL)
    
    ScalatorCommutationis.Add((spatium, 0))
   
    edCommuta = ego.edCommuta = wx.TextCtrl(ego)
    ScalatorCommutationis.Add(edCommuta, 2, wx.GROW)
    
    ScalatorSinister.Add((0, spatium))

    # transposor (přepínač)
    trHonoraCasum = ego.trHonoraCasum = wx.CheckBox(ego, 
      label=u"&Honora casum")
    ScalatorSinister.Add(trHonoraCasum, 1, wx.GROW)

    ScalatorSinister.Add((0, spatium))

    trVerbaCompleta = ego.trVerbaCompleta = wx.CheckBox(ego,
      label=u"&Verba completa modo")
    ScalatorSinister.Add(trVerbaCompleta, 1, wx.GROW)
    
    ScalatorCapitalis.Add((spatium, 0))

    ScalatorPressorum = wx.BoxSizer(wx.VERTICAL)
    ScalatorCapitalis.Add(ScalatorPressorum, 1, wx.GROW)

    prQuaere = ego.prQuaere = wx.Button(ego, label=u"&Quaere")
    ScalatorPressorum.Add(prQuaere, 1, wx.GROW)
    
    ScalatorPressorum.Add((0, spatium))
    
    prCommuta = ego.prCommuta = wx.Button(ego, label=u"&Commuta")
    ScalatorPressorum.Add(prCommuta, 1, wx.GROW)
    
    ScalatorPressorum.Add((0, spatium))

    prCommutaOmnia = ego.prCommutaOmnia = wx.Button(ego, 
      label=u"Commuta &omnia")
    ScalatorPressorum.Add(prCommutaOmnia, 1, wx.GROW)
    
    ScalatorPressorum.Add((0, spatium))
    
    prConclude = ego.prConclude = wx.Button(ego,
      id=wx.ID_CANCEL, label=u"C&ancellare")
    ScalatorPressorum.Add(prConclude, 1, wx.GROW) 
    
    ScalatorMarginalis.Fit(ego)
    
    # eventus
    ego.quaestio = None
    ego.commutatio = None
    ego.commutatio_omnium = None
    ego.conclusio = None
  
    ego.Bind(wx.EVT_BUTTON, ego.CumCompressionePressorisQuaerendi, prQuaere)
    ego.Bind(wx.EVT_BUTTON, ego.CumCompressionePressorisCommutandi, prCommuta)
    ego.Bind(wx.EVT_BUTTON, ego.CumCompressionePressorisCommutandiOmnium, 
      prCommutaOmnia)
    ego.Bind(wx.EVT_BUTTON, ego.CumCompressionePressorisConcludendi, prConclude)
    
  def CumCompressionePressorisQuaerendi(ego, evt):
    if ego.quaestio:
      if ego.Parens:
        #ego.quaestio.__call__(evt)
        ego.quaestio(evt)
      else:
        ego.quaestio(evt)
    evt.Skip()

  def CumCompressionePressorisCommutandi(ego, evt):
    if ego.commutatio:
      if ego.Parens:
        ego.commutatio.__call__(evt)
      else:
        ego.commutatio(evt)
    evt.Skip()

  def CumCompressionePressorisCommutandiOmnium(ego, evt):
    if ego.commutatio_omnium:
      if ego.Parens:
        ego.commutatio_omnium.__call__(evt)
      else:
        ego.commutatio_omnium(evt)
    evt.Skip()

  def CumCompressionePressorisConcludendi(ego, evt):
    if ego.conclusio:
      if ego.Parens:
        ego.conclusio.__call__(evt)
      else:
        ego.conclusio(evt)
    evt.Skip()
    
  @Property
  def Indicationes():
    """Indicationes: integer
    Indicant si casum honorare et si verba tam completa quaerere."""
    def fget(ego):
      return (ego.trHonoraCasum.GetValue() * 4 | 
              ego.trVerbaCompleta.GetValue() * 2)
    def fset(ego, inds):
      ego.trHonoraCasum.SetValue(bool(inds and 4))
      ego.trVerbaCompleta.SetValue(bool(inds and 2))
      

  @Property
  def TextumQuaerendum():
    def fget(ego):
      return ego.edQuaere.GetValue()
    def fset(ego, textum):
      ego.edQuaere.SetValue(textum)
    
  @Property
  def TextumCommutandum():
    def fget(ego):
      return ego.edCommuta.GetValue()
    def fset(ego, textum):
      ego.edCommuta.SetValue(textum)

  
    
if __name__ == '__main__':
  app = wx.App()
  dq = DialogusQuaestionis(None)
  #app.SetTopWindow(dq)
  dq.ShowModal()
