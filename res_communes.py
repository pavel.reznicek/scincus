#!/usr/bin/env python
# -*- coding: utf8 -*-

import os
import sys
import locale
from os import path
import wx
from cigydd.irange import *
from cigydd.properties import Property
obiectum = object
nil = None

def sysenc(s):
  return s.encode(sys.getfilesystemencoding())

def sysdec(s): 
  return s.decode(sys.getfilesystemencoding())

#def cp1250(s):
#  return s.encode('cp1250')

def utf8enc(s):
  return s.encode('utf-8')

def utf8dec(s):
  return s.decode('utf-8')

def texenc(s): 
  """functio ad translationem nominorum foliorum in sequentias 
  effugitas TeXi"""
  resultum = u''
  if type(s) == unicode:
    s = utf8enc(s)
  #if type(s) == str:
  #  s = s.decode('utf-8')
  for c in s:
    if ord(c) not in range(0x21, 0x80): # 33..127
      resultum += hex(ord(c)).replace('0x', '^^')
    else:
      resultum += c
  return resultum
#ellipsis = unichr(0x2026) # puncta tria: ...

CodiSignorumAlphanumericorum = \
    irange(ord(u"a"), ord(u"z")) + \
    irange(ord(u"A"), ord(u"Z")) + \
    irange(ord(u"0"), ord(u"9"))
SignaAlphanumerica = u"".join(map(unichr, CodiSignorumAlphanumericorum))

CodiSignorumAlphaicorum = \
    irange(ord(u"a"), ord(u"z")) + \
    irange(ord(u"A"), ord(u"Z"))
SignaAlphaica = u"".join(map(unichr, CodiSignorumAlphaicorum))

import UserString
ustr = u"".join(map(unichr, irange(1, 0xffff)))
unicodi=UserString.MutableString(ustr)
for sig in SignaAlphanumerica+u'\\':
    ind=unicodi.find(sig)
    unicodi[ind]=u''
SignaNonAlphanumerica = unicodi.data
del(unicodi, ustr)
del(UserString)

if __name__=='__main__':
    import __main__ as mn
    if mn.__file__.endswith('res_communes.py'):
      ViaNostra=sysdec(path.dirname(path.realpath(mn.__file__)))
    else: # snad jen pro účely ladění v Boa Constructoru
      ViaNostra=sysdec('/home/pavel/Tvorba/Python/scincus')
else:
##  try:
##    import res_communes as scriptus
##    ViaNostra=path.dirname(path.realpath(scriptus.__file__))
##  except ImportError:
##    ViaNostra=sys.path[0]
##  ViaNostra = path.dirname(sys.argv[0])
  ViaNostra = sysdec(sys.path[0])
if ViaNostra == '': ViaNostra = u'.'

# Folium cum tabula mandatorum

ViaAdDataMandatorum=ViaNostra+os.sep+"tabula mandatorum.txt"
#print u"Via ad data mandatorum: %s"%(ViaAdDataMandatorum)

# Refinitio viae datae (v)
def ViaVera(v):
  v = path.expandvars(v)
  v = path.expanduser(v)
  v = path.normpath(v)
  v = path.realpath(v)
  return v


class Mandator(obiectum):
  def LegeMandata(ego):
    if not path.exists(ViaAdDataMandatorum):
      nuntius = u'Folium cum tabula mandatorum non inquaesitum est. Scribo novum.'
      wx.MessageBox(nuntius)
      print nuntius
      try:
        f=open(ViaAdDataMandatorum, 'wb+')
        f.write(u"\\TeX" + os.linesep) # mandatum unum pro exemplo
        f.close()
      except:
        nuntius = u'Non possum scribere folium mandatorum novum. '\
          'Depreco te scribere tibi unum in via %s%s.' % (
            os.linesep, ViaAdDataMandatorum
            )
        wx.MessageBox(nuntius)
        print nuntius
    FoliumMandatorum = open(ViaAdDataMandatorum)
    ego.TabulaMandatorum = [linea.rstrip() for linea in FoliumMandatorum if linea]
    FoliumMandatorum.close()
    ego.TabulaMandatorum.sort(locale.strcoll)
    ego.Mandata=unichr(11).join(ego.TabulaMandatorum)
    #return Mandata

mandator = Mandator()

persona_folii=ur"Folia TeXnica (tex,sty,cls)|*.tex;*.sty;*.cls|Folia omnia (*.*)|*.*"

def DialogusFolii(
  parens,
  via_directorii,
  via_folii,
  persona=persona_folii,
  titulus=u'Salvare folium',
  stylus=0
):
  fd=wx.FileDialog(parens,titulus,via_directorii,via_folii,
    wildcard=persona,style=stylus)
  return fd
  
def DialogusSalvationis(
  parens,
  via_directorii,
  via_folii,
  persona=persona_folii
):
  fd=DialogusFolii(parens,via_directorii,via_folii,persona,
    titulus=u'Salvare folium',stylus=wx.SAVE#|wx.HIDE_READONLY
  )
  return fd

def DialogusReserationis(
  parens,
  via_directorii,
  via_folii,
  persona=persona_folii
):
  fd=DialogusFolii(parens,via_directorii,via_folii,persona,
    titulus=u'Reserare folium',stylus=wx.OPEN#|wx.HIDE_READONLY
  )
  return fd

class GeneratorDatorumQuaestionis(obiectum):
  @Property
  def Data():
    def fget(ego):
      data = wx.FindReplaceData(ego.Indicationes)
      data.SetFindString(unicode(ego.TextumQuaerendum))
      data.SetReplaceString(unicode(ego.TextumCommutandum))
      return data
    def fset(ego, data):
      ego.Indicationes = data.GetFlags()
      ego.TextumQuaerendum = unicode(data.GetFindString())
      ego.TextumCommutandum = unicode(data.GetReplaceString())
  @Property
  def Indicationes():
    def fget(ego):
      if hasattr(ego, 'indicationes'):
        return ego.indicationes
      else:
        return 0
    def fset(ego, indicationes):
      ego.indicationes = indicationes
  @Property
  def TextumQuaerendum():
    def fget(ego):
      if hasattr(ego, 'tq'):
        return unicode(ego.tq)
      else:
        return u''
    def fset(ego, tq):
      ego.tq = unicode(tq)
  @Property
  def TextumCommutandum():
    def fget(ego):
      if hasattr(ego, 'tc'):
        return unicode(ego.tc)
      else:
        return u''
    def fset(ego, tc):
      ego.tc = unicode(tc)

if wx.Platform=='__WXMSW__':
    PraedicatusPlatformae=u'Fenestralis'
else:
    PraedicatusPlatformae=u'Linucalis'

################
## constantes ##
################
InscriptioFoliiNovi=u"<folium novum>"
# signi quotationis centroeuropaeicae
oq=u'\u201e' # signum originis quotationis
fq=u'\u201c' # signum finis quotationis