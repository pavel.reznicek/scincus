#!/usr/bin/env python
# -*- coding: utf8 -*-

import wx
#import wx.xrc
from res_communes import *
from xml.dom import minidom

#class XMLConfig(wx.xrc.XmlDocument):
class XMLConfig(object):
  def __init__(ego, via):
    #wx.xrc.XmlDocument.__init__(ego, via)
    #ego._root = ego.Root
    #print ego._root
    ego._doc = minidom.parse(via)
    
  def __del__(ego):
    if hasattr(ego,"_doc"): ego._doc.unlink()
  
  def GetChildByName(ego, node, name):
    if not node:
      return None
    #children = node.GetChildren()
    children = node.childNodes
    #child = children#.GetFirst()
    #while child:
    for child in children:
      #if hasattr(child, "Name"):
      if child.nodeName == name:
        #print child.Name
        return child
      #child = child.GetNext()
  
  def GetPathNode(ego, via):
    partes = via.split(u'/')
    #parens = ego._root # Mech
    parens = ego._doc.documentElement
    for pars in partes[1:]: 
      infans = ego.GetChildByName(parens, pars)
      parens = infans
      if not infans:
        break
    if infans:
      return infans
  
  def CreatePathNode(ego, via):
    partes = via.split('/')
    #parent = ego._root
    parens = ego._doc.documentElement
    for pars in partes[1:]:
      infans = ego.GetChildByName(parens, pars)
      if not infans:
        #child = wx.xrc.XmlNode(parent, 
        #  type=wx.xrc.XML_ELEMENT_NODE,name=part
        #  )
        child = wx.xrc.XmlNodeEasy(wx.xrc.XML_ELEMENT_NODE, part)
        parent.AddChild(child)
        infans = ego._doc.createElement(pars)
        parens.appendChild(infans)
        #infans = parens
      parens = infans
    return infans

  def QuaereLapidulamTexticam(ego, elementum):
    if elementum:
      #infantes = [infans for infans in elementum.getChildNodes \
      #  if infans.nodeType == minidom.Node.TEXT_NODE]
      #if infantes:
      #  return infantes[0]
      #else:
      #  return None
      for infans in elementum.childNodes:
        if infans.nodeType == minidom.Node.TEXT_NODE:
          return infans
      # pokud smyčka nic nenašla, a tedy proběhla celá, dostaneme se až sem:
      return None

  def Lege(ego, via, eveniens=u""):
    lapidula = ego.GetPathNode(via)
    if lapidula:
      #print node.GetNodeContent()
      #return node.GetNodeContent()
      #for infans in lapidula.getChildNodes:
      #  if infans.nodeType == minidom.Node.TEXT_NODE:
      #    return infans.nodeValue
      ## pokud smyčka nic nenašla, a tedy proběhla celá, dostaneme se až sem:
      #return eveniens
      textus = ego.QuaereLapidulamTexticam(lapidula)
      if textus:
        return textus.nodeValue
      else:
        return eveniens
    else:
      return eveniens
  
  def Scribe(ego, via, contentum):
    node = ego.CreatePathNode(via)
    #node = ego.GetPathNode(via)
    lapidula = ego.CreatePathNode(via)
    #if node:
    #  print node, node.Name
    #  #node.Content = contens
    #  txt = wx.xrc.XmlNodeEasy(type=wx.xrc.XML_TEXT_NODE, name="Textus")
    #  txt.Content = contentum
    #  node.AddChild(txt)
    #  #node.AddChild(wx.xrc.XmlNodeEasy(
    #  #  type=wx.xrc.XML_TEXT_NODE, name="Textus"))
    #  return True
    #else:
    #  return False
    if lapidula:
      textus = ego.QuaereLapidulamTexticam(lapidula)
      if not textus:
        textus = ego._doc.createTextNode(contentum)
        lapidula.appendChild(textus)
      else:
        textus.nodeValue = contentum
      return True
    else:
      return False
  
  def _DaDocumentum(ego):
    return ego._doc
  def _ObiciaDocumentum(ego, documentum):
    ego._doc = documentum
  Documentum = property(_DaDocumentum, _ObiciaDocumentum)
  
  def Salva(ego, via):
    ego.Documentum.writexml(open(via, "wb+"))
    # Hezké XML, ale načítají se i tabulátory a odstavce
    #XML = ego.Documentum.toprettyxml()
    #open(via, "wb+").write(XML, encoding=u"UTF-8")
    
  def Resera(ego, via):
    if hasattr(ego,"_doc"): ego._doc.unlink()
    ego._doc = minidom.parse(via)
    
class ObicionesScinci(obiectum):
  def __init__(ego):
    #ViaAdIni=ego._ViaAdIni = ViaNostra + os.sep + u'scincus.ini'
    ViaAdIni=ego._ViaAdIni = ViaNostra + os.sep + u'scincus.xml'
    #ini = ego._ini = wx.FileConfig(
    #  localFilename=ViaAdIni,
    #  style=wx.CONFIG_USE_LOCAL_FILE
    #  )
    ini = ego._ini = XMLConfig(ViaAdIni)
    #print ViaAdIni
  
  def _DaIni(ego):
    return ego._ini
  ini=property(_DaIni)
  
  def Relege(ego):
    #ini = ego._ini = wx.FileConfig(
    #  localFilename=ego.ViaAdIni,
    #  style=wx.CONFIG_USE_LOCAL_FILE
    #  )
    ini = ego._ini = XMLConfig(ego._ViaAdIni)

  def _DaViamAdIni(ego):
    return ego._ViaAdIni
  ViaAdIni=property(_DaViamAdIni)
  
  def _DaNominemScripturae(ego):
    #nomen=ego.ini.Read(u'/Styli/NomenScripturae')
    nomen=ego.ini.Lege(u'/Styli/NomenScripturae')
    return nomen
  def _ObiciaNominemScripturae(ego,nomen):
    #ego.ini.Write(u'/Styli/NomenScripturae',nomen)
    #ego.ini.Flush()
    ego.ini.Scribe(u'/Styli/NomenScripturae',nomen)
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)
  NomenScripturae=property(_DaNominemScripturae,_ObiciaNominemScripturae)
  
  def _DaMagnitudinemScripturae(ego):
    #magnitudo=ego.ini.ReadInt(u'/Styli/MagnitudoScripturae')
    lectum = ego.ini.Lege(u'/Styli/MagnitudoScripturae',u"9")
    magnitudo=int(lectum)
    return magnitudo
  def _ObiciaMagnitudinemScripturae(ego,magnitudo):
    #ego.ini.WriteInt(u'/Styli/MagnitudoScripturae',magnitudo)
    #ego.ini.Flush()
    ego.ini.Scribe(u'/Styli/MagnitudoScripturae',magnitudo)
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)
  MagnitudoScripturae=property(
    _DaMagnitudinemScripturae,_ObiciaMagnitudinemScripturae
    )
    
  def _DaStylos(ego):
    #ego.ini.SetPath(u'/Styli')
    #sequens=0
    #styli=[]
    #styli=[{"ColorTexti":None,"ColorPosterior":None} for i in rozsah(255)]
    #for i in range(ego.ini.GetNumberOfEntries()):
    #  flagum,nomen,sequens=ego.ini.GetNextEntry(sequens)
    #  if nomen.startswith("ColorPosterior") \
    #  or nomen.startswith("ColorTexti"):
    #    if nomen.startswith("ColorPosterior"):
    #      NumerusStyli=int(nomen.replace("ColorPosterior","",1))
    #    elif nomen.startswith("ColorTexti"):
    #      NumerusStyli=int(nomen.replace("ColorTexti","",1))
    #    valus=ego.ini.Read(nomen)
    #    color=wx.ColorRGB(int(valus.replace("$","0x",1),16))
    #    if nomen.startswith("ColorPosterior"):
    #      styli[NumerusStyli]["ColorPosterior"]=color.Get()
    #    elif nomen.startswith("ColorTexti"):
    #      styli[NumerusStyli]["ColorTexti"]=color.Get()
    #ego._styli=styli
    #return styli
    
    sequens=0
    styli=[]
    styli=[{"ColorTexti":None,"ColorPosterior":None} for i in irange(255)]
    parens = ego.ini.GetPathNode(u"/Styli")
    infantes = parens.childNodes
    for i in infantes:
    #i = infantes
    #while i:
      nomen = i.nodeName
      if nomen.startswith("ColorPosterior"):
        NumerusStyli=int(nomen.replace("ColorPosterior","",1))
      elif nomen.startswith("ColorTexti"):
        NumerusStyli=int(nomen.replace("ColorTexti","",1))
      if nomen.startswith("ColorPosterior") \
      or nomen.startswith("ColorTexti"):
        #valor=ego.ini.Read(nomen)
        #valor=ego.ini.Lege(nomen)
        #valor = i.GetNodeContent()
        textus = ego._ini.QuaereLapidulamTexticam(i)
        valor = textus.nodeValue
        color=wx.ColourRGB(int(valor.replace("$","0x",1),16))
        #color = wx.Colour()
        #color.SetRGB(int(valor.replace("$","0x",1),16))
      if nomen.startswith("ColorPosterior"):
        styli[NumerusStyli]["ColorPosterior"]=color.Get()
      elif nomen.startswith("ColorTexti"):
        styli[NumerusStyli]["ColorTexti"]=color.Get()
      #i = i.Next
    ego._styli=styli
    return styli
  Styli=property(_DaStylos,
    doc=u'Styli fenestrinarum textificientium stylatarum'
    )
    
  def _DaFoliumRecentem(ego):
    #return ego.ini.Read(u'/Miscellanea/FoliumRecens'+PraedicatusPlatformae)
    return ego.ini.Lege(u'/Miscellanea/FoliumRecens'+PraedicatusPlatformae)
  def _ObiciaFoliumRecentem(ego,folium):
    #ego.ini.Write(u'/Miscellanea/FoliumRecens'+PraedicatusPlatformae,folium)
    #ego.ini.Flush()
    ego.ini.Scribe(u'/Miscellanea/FoliumRecens'+PraedicatusPlatformae,folium)
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)

  FoliumRecens=property(_DaFoliumRecentem,_ObiciaFoliumRecentem)
  
  def _DaSessionem(ego):
    lectum = ego.ini.Lege(u'/Miscellanea/Sessio'+PraedicatusPlatformae,u'[]')
    if lectum:
      sessio = eval(
        #ego.ini.Read(u'/Miscellanea/Sessio'+PraedicatusPlatformae,u'[]')
        lectum
        )
      return [ViaVera(f) for f in sessio]
  def _ObiciaSessionem(ego,sessio):
    #ego.ini.Write(u'/Miscellanea/Sessio'+PraedicatusPlatformae,unicode(sessio))
    #ego.ini.Flush()
    ego.ini.Scribe(u'/Miscellanea/Sessio'+PraedicatusPlatformae,unicode(sessio))
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)
  Sessio=property(_DaSessionem,_ObiciaSessionem)
  
  def _DaPositionemFenestri(ego):
    #return eval(ego.ini.Read(u'/Miscellanea/PositioFenestri', u'(0, 0)'))
    return eval(ego.ini.Lege(u'/Miscellanea/PositioFenestri', u'(0, 0)'))
  def _ObiciaPositionemFenestri(ego, positio):
    #ego.ini.Write(u'/Miscellanea/PositioFenestri', str(positio))
    #ego.ini.Flush()
    ego.ini.Scribe(u'/Miscellanea/PositioFenestri', str(positio))
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)
  PositioFenestri=property(_DaPositionemFenestri, _ObiciaPositionemFenestri)

  def _DaMagnitudinemFenestri(ego):
    #return eval(ego.ini.Read(u'/Miscellanea/MagnitudoFenestri',u'(640,480)'))
    return eval(ego.ini.Lege(u'/Miscellanea/MagnitudoFenestri',u'(640,480)'))
  def _ObiciaMagnitudinemFenestri(ego,magnitudo):
    #ego.ini.Write(u'/Miscellanea/MagnitudoFenestri',str(magnitudo))
    #ego.ini.Flush()
    ego.ini.Scribe(u'/Miscellanea/MagnitudoFenestri',str(magnitudo))
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)
  MagnitudoFenestri=property(_DaMagnitudinemFenestri,_ObiciaMagnitudinemFenestri)
  
  def _DaCompilatorem(ego):
    #return ego.ini.Read(u'/Miscellanea/Compilator'+PraedicatusPlatformae,u'cslatex')
    return ego.ini.Lege(u'/Miscellanea/Compilator'+PraedicatusPlatformae,u'cslatex')
  def _ObiciaCompilatorem(ego,comp):
    #ego.ini.Write(u'/Miscellanea/Compilator'+PraedicatusPlatformae,insp)
    ego.ini.Scribe(u'/Miscellanea/Compilator'+PraedicatusPlatformae,insp)
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)
  Compilator=property(_DaCompilatorem,_ObiciaCompilatorem)

  def _DaInspectorem(ego):
    if wx.Platform=='__WXMSW__':
      #return ego.ini.Read(u'/Miscellanea/Inspector'+PraedicatusPlatformae,u'start owindvi')
      return ego.ini.Lege(u'/Miscellanea/Inspector'+PraedicatusPlatformae,u'start owindvi')
    else:
      #return ego.ini.Read(u'/Miscellanea/Inspector'+PraedicatusPlatformae,u'xdvi')
      return ego.ini.Lege(u'/Miscellanea/Inspector'+PraedicatusPlatformae,u'xdvi')
  def _ObiciaInspectorem(ego,insp):
    #ego.ini.Write(u'/Miscellanea/Inspector'+PraedicatusPlatformae,insp)
    ego.ini.Scribe(u'/Miscellanea/Inspector'+PraedicatusPlatformae,insp)
    ego.ini.Salva(ego.ViaAdIni)
    #ego.ini.Load(ego.ViaAdIni)
  Inspector=property(_DaInspectorem,_ObiciaInspectorem)

app=wx.App(redirect=False)
obi=ObicionesScinci()
#mag = obi.MagnitudoScripturae
#obi.Inspector = u"okular"
#print obi.Styli