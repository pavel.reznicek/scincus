# Scincus
A TeX/LaTeX editor written in Python

This is one of the results of my effort to write a custom TeX editor.

As I started with programming, I was fascinated by the code completion tools available in the VisualBasic and Delphi IDEs. 
And, as I started with TeX, I wanted to have a similar user-friendly environment for it.

So I wrote Scincus. Well, it wasn’t so straightforward. I tried to build my own custom-drawn code editor control
in Delphi but that task was far beyond my abilities. I finished it to some state but the method I used was basically wrong 
(using an image control to draw the text is a bottleneck and the drawing is slow even on good GPUs).

Maybe I’ll publish the code once but for now, you have another result, which is Scincus, an editor based on Python, wxPython, 
and wxPython’s styled text control, which is based on the advanced Scintilla text control.

The GUI is written in Latin. Pretty weird you can think but I just tried to introduce another standard than the nowadays
widespread English. The code is also written in Latin except of the necessary syntax keywords, which are in English. 
I know my Latin is poor but I had great fun coding in such a way.

Over time, I found Lazarus FreePascal IDE and its SynEdit code editing control and wrote another TeX/LaTeX editor,
named Synek, because I found some bugs in wxPython’s StyledTextControl that made the editing experience ugly. 
I got used to Synek and stopped using Scincus, and also developing it. Because of that, Scincus has some features that can be
considered “bugs from old style”. See Known issues.

Known Issues
============
- Scincus supports only the Central European Windows code page, CP1250, for file editing. This issue has a great priority 
  in the future development. Planning to switch to UTF-8 (Unicode).
- The image resource files have names in Czech. Planning to rename them to Latin.

Installation & Dependencies
===========================
- You have to have Python 2, best Python 2.7, installed on your system. See https://www.python.org.
- You have to have wxPython, best v2.9.x, installed on your system. See https://wxpython.org.
- The “cigydd” package, a. k. a. the Cigydd’s Python Library. See https://github.com/Cigydd/cigydd.

Running
=======
Once all dependencies have been installed, you can run the program by double-clicking the file “scincus.py” 
or by issuing the command “python scincus.py [file…]” from the command line. Running it from a terminal is highly recommended
so that you could respond to (La)TeX’s questions.

Plans
=====
- Transition to Python 3 and wxPython 3.x/4.x (shouldn't be hard)
- Transition to UTF-8 editation (shouldn't be hard too)
- Visualisation of the configuration settings; now they get edited in plain text—not much user friendly
